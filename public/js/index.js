$(function () {
  $("#load-imgs")
    .imagesLoaded()
    .always(function (e) {
      if (e.isComplete) {
        TweenMax.to("#loading", 0.8, {
          autoAlpha: 0,
          display: "none",
          ease: Linear.easeNone,
        });
        AOS.refresh();
        init.play();
      }
    });

  var init = new TimelineMax();
  init
    .from(".kv-slogan .img", 2, {
      rotationY: -360,
      ease: Quart.easeOut,
      autoAlpha: 0,
    })
    .from(
      ".kv-slogan-txt .img",
      2,
      {
        y: -30,
        ease: Back.easeOut,
        autoAlpha: 0,
      },
      "-=.8"
    )
    .from(
      ".kv-peo",
      1,
      {
        y: 30,
        ease: Back.easeOut,
        autoAlpha: 0,
      },
      "-=2"
    )
    .from(
      ".kv-cha1",
      0.5,
      {
        y: 30,
        ease: Back.easeOut,
        autoAlpha: 0,
      },
      "-=1.8"
    )
    .from(
      ".kv-cha2",
      0.5,
      {
        y: 30,
        ease: Back.easeOut,
        autoAlpha: 0,
      },
      "-=1.8"
    )
    .from(
      ".kv-cha3",
      0.5,
      {
        y: 30,
        ease: Back.easeOut,
        autoAlpha: 0,
      },
      "-=1.8"
    )
    .from(
      ".kv-cha4",
      1,
      {
        x: 30,
        scale: 0.8,
        ease: Quart.easeOut,
        autoAlpha: 0,
      },
      "-=1.8"
    )
    .from(
      ".kv-cha5",
      1,
      {
        x: 30,
        scale: 0.8,
        ease: Quart.easeOut,
        autoAlpha: 0,
      },
      "-=1.4"
    );

  init.pause();

  AOS.init();

  $("a.map-link").hover(
    function () {
      var getShowItem = $(this).attr("href");
      $("#area-map").find(getShowItem).addClass("active");
      console.log(getShowItem);
    },
    function () {
      $(".area-item.active").removeClass("active");
    }
  );

  // Instantiate new modal
  var modal = new Custombox.modal({
    content: {
      effect: "fadein",
      target: "#modal-ticket",
    },
  });

  $("#btn-ticket").click(function (e) {
    e.preventDefault();
    modal.open();
  });
});
