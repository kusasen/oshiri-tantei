$(function () {
  var _lockBody = $("body");

  $("#mob-navi").click(function (e) {
    e.preventDefault();
    $(this).toggleClass("active");
    if ($(this).hasClass("active")) {
      $("#mob-navis").addClass("animation");
      TweenMax.to("#mob-navis", 0.8, {
        autoAlpha: 1,
        left: 0,
        display: "block",
        ease: Quart.easeOut,
      });
      $("html").addClass("lock-body");
    } else {
      $("#mob-navis").removeClass("animation");
      TweenMax.to("#mob-navis", 0.8, {
        autoAlpha: 0,
        left: "-40%",
        display: "none",
        ease: Quart.easeOut,
      });
      $("html").removeClass("lock-body");
    }
  });

  $("#mob-navis a").click(function () {
    $("#mob-navi").removeClass("active");
    $("#mob-navis").removeClass("animation");
    TweenMax.to("#mob-navis", 0.8, {
      autoAlpha: 0,
      left: "-40%",
      display: "none",
      ease: Quart.easeOut,
    });
    $("html").removeClass("lock-body");
  });

  $(".linkTo").click(function (e) {
    e.preventDefault();
    var getSection = $(this).attr("href");
    if ($(window) < 740) {
      $("#mob-navi").click();
    }

    $("body,html").animate({ scrollTop: $(getSection).offset().top }, 800);
  });
});
